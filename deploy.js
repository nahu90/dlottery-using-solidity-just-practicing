const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');

const { interface, bytecode } = require('./compile');

const provider = new HDWalletProvider(
    'february normal regular pumpkin search degree usage draw force decade feature easy',
    'https://rinkeby.infura.io/v3/8c0e29031e95488bb32d1477ed1eeeb4'
);

const web3 = new Web3(provider);

const deploy = async () => {
    // Get a list of all accounts
    const accounts = await web3.eth.getAccounts();
    console.log('Attempting to deploy from account ', accounts[0]);

    const result = await new web3.eth.Contract(JSON.parse(interface))
      .deploy({
          data: bytecode
      })
      .send({
          from: accounts[0],
          gas: '1000000'
      });

    //inbox.setProvider(provider);
    console.log('Contract deployed to', result.options.address);
};

deploy();
